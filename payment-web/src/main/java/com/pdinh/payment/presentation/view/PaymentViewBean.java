package com.pdinh.payment.presentation.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.annotation.Resource;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 * @author BegK
 * 
 */
@ManagedBean(name = "paymentViewBean")
@ViewScoped
public class PaymentViewBean implements Serializable {

	@Resource(mappedName = "java:global/application/payment/config_properties")
	private Properties configProperties;

	private static final long serialVersionUID = 1L;

	private String apiKey;
	private String serviceHost;
	private String tokenValue;

	private String clientName;
	private List<String> invoiceNumbers = new ArrayList<String>();
	private String amount;

	private String nameOnCard;
	private String billAddress1;
	private String billAddress2;
	private String billAddress3;
	private String billCountry;
	private String billState;
	private String billCityTown;
	private String billZipCode;
	private String cardType;
	private String cardNumber;
	private String cardCvC;
	private String expMonth;
	private String expYear;
	private String otherComments;

	private String contactName;
	private String contactPhone;
	private String contactEmail;
	// private boolean sameAsBillingAddr;
	private String contactAddress1;
	private String contactAddress2;
	private String contactAddress3;
	private String contactCountry;
	private String contactState;
	private String contactCityTown;
	private String contactZipCode;

	public String getApiKey() {
		return configProperties.getProperty("paymentServerApiKey");
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	public String getServiceHost() {
		return configProperties.getProperty("paymentServiceHost");
	}

	public void setServiceHost(String serviceHost) {
		this.serviceHost = serviceHost;
	}

	public String getTokenValue() {
		return tokenValue;
	}

	public void setTokenValue(String tokenValue) {
		this.tokenValue = tokenValue;
	}

	public Properties getConfigProperties() {
		return configProperties;
	}

	public void setConfigProperties(Properties configProperties) {
		this.configProperties = configProperties;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public List<String> getInvoiceNumbers() {
		return invoiceNumbers;
	}

	public void setInvoiceNumbers(List<String> invoiceNumbers) {
		this.invoiceNumbers = invoiceNumbers;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getNameOnCard() {
		return nameOnCard;
	}

	public void setNameOnCard(String nameOnCard) {
		this.nameOnCard = nameOnCard;
	}

	public String getBillAddress1() {
		return billAddress1;
	}

	public void setBillAddress1(String billAddress1) {
		this.billAddress1 = billAddress1;
	}

	public String getBillAddress2() {
		return billAddress2;
	}

	public void setBillAddress2(String billAddress2) {
		this.billAddress2 = billAddress2;
	}

	public String getBillAddress3() {
		return billAddress3;
	}

	public void setBillAddress3(String billAddress3) {
		this.billAddress3 = billAddress3;
	}

	public String getBillCountry() {
		return billCountry;
	}

	public void setBillCountry(String billCountry) {
		this.billCountry = billCountry;
	}

	public String getBillState() {
		return billState;
	}

	public void setBillState(String billState) {
		this.billState = billState;
	}

	public String getBillCityTown() {
		return billCityTown;
	}

	public void setBillCityTown(String billCityTown) {
		this.billCityTown = billCityTown;
	}

	public String getBillZipCode() {
		return billZipCode;
	}

	public void setBillZipCode(String billZipCode) {
		this.billZipCode = billZipCode;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getCardCvC() {
		return cardCvC;
	}

	public void setCardCvC(String cardCvC) {
		this.cardCvC = cardCvC;
	}

	public String getExpMonth() {
		return expMonth;
	}

	public void setExpMonth(String expMonth) {
		this.expMonth = expMonth;
	}

	public String getExpYear() {
		return expYear;
	}

	public void setExpYear(String expYear) {
		this.expYear = expYear;
	}

	public String getOtherComments() {
		return otherComments;
	}

	public void setOtherComments(String otherComments) {
		this.otherComments = otherComments;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getContactPhone() {
		return contactPhone;
	}

	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	// public boolean isSameAsBillingAddr() {
	// return sameAsBillingAddr;
	// }

	// public void setSameAsBillingAddr(boolean sameAsBillingAddr) {
	// this.sameAsBillingAddr = sameAsBillingAddr;
	// }

	public String getContactAddress1() {
		return contactAddress1;
	}

	public void setContactAddress1(String contactAddress1) {
		this.contactAddress1 = contactAddress1;
	}

	public String getContactAddress2() {
		return contactAddress2;
	}

	public void setContactAddress2(String contactAddress2) {
		this.contactAddress2 = contactAddress2;
	}

	public String getContactAddress3() {
		return contactAddress3;
	}

	public void setContactAddress3(String contactAddress3) {
		this.contactAddress3 = contactAddress3;
	}

	public String getContactCountry() {
		return contactCountry;
	}

	public void setContactCountry(String contactCountry) {
		this.contactCountry = contactCountry;
	}

	public String getContactState() {
		return contactState;
	}

	public void setContactState(String contactState) {
		this.contactState = contactState;
	}

	public String getContactCityTown() {
		return contactCityTown;
	}

	public void setContactCityTown(String contactCityTown) {
		this.contactCityTown = contactCityTown;
	}

	public String getContactZipCode() {
		return contactZipCode;
	}

	public void setContactZipCode(String contactZipCode) {
		this.contactZipCode = contactZipCode;
	}

}
