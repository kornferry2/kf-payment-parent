package com.pdinh.payment.presentation.controller;

import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import org.apache.http.HttpException;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdinh.payment.presentation.view.PaymentViewBean;

/**
 * @author BegK
 * 
 */
@ManagedBean(name = "paymentControllerBean")
@RequestScoped
public class PaymentControllerBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(PaymentControllerBean.class);

	private static final String TOKEN_VALUE = "token_value";
	private static final String CLIENT_NAME = "client_name";
	private static final String AMOUNT = "amount";
	private static final String NAME_ON_CARD = "name_on_card";
	private static final String BILL_ADDR_1 = "bill_address_1";
	private static final String BILL_ADDR_2 = "bill_address_2";
	private static final String BILL_ADDR_3 = "bill_address_3";
	private static final String BILL_COUNTRY = "bill_country";
	private static final String BILL_STATE = "bill_state";
	private static final String BILL_CITY = "bill_city";
	private static final String BILL_ZIP = "bill_zip";
	private static final String CARD_TYPE = "card_type";
	private static final String CARD_NUMBER = "card_number";
	private static final String CARD_CVC = "card_cvc";
	private static final String EXP_MONTH = "exp_month";
	private static final String EXP_YEAR = "exp_year";
	private static final String OTHER_COMMENTS = "other_comments";
	private static final String CONTACT_NAME = "contact_name";
	private static final String CONTACT_PHONE = "phone";
	private static final String CONTACT_EMAIL = "email";
	// private static final String SAME_AS_BILLING = "sameasbilling";
	private static final String CONTACT_ADDR_1 = "address_1";
	private static final String CONTACT_ADDR_2 = "address_2";
	private static final String CONTACT_ADDR_3 = "address_3";
	private static final String CONTACT_COUNTRY = "country";
	private static final String CONTACT_STATE = "state";
	private static final String CONTACT_CITY = "city";
	private static final String CONTACT_ZIP = "zip";

	@Resource(mappedName = "java:global/application/payment/config_properties")
	private Properties configProperties;

	private String servletaddress = "/paymentserviceweb/HPSAPIServlet";

	@ManagedProperty(name = "paymentViewBean", value = "#{paymentViewBean}")
	private PaymentViewBean paymentViewBean;

	private Boolean firstTime = true;

	public void initView() {
		if (firstTime) {

			firstTime = false;
		}

		log.debug("firstTime {}", firstTime);

	}

	public String processForm() {
		log.debug("Inside submitForm");

		HttpClient httpClient = new DefaultHttpClient();
		HttpPost httpPost = null;
		String redirectURI = null;

		try {

			httpPost = new HttpPost("http://" + paymentViewBean.getServiceHost() + servletaddress);
			// Request parameters and other properties.

			List<NameValuePair> params = prepareFormData();

			httpPost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			log.debug("UnsupportedEncodingException: " + e.getMessage());
			e.printStackTrace();
		}
		/*
		 * Execute the HTTP Request
		 */
		try {
			HttpResponse response = httpClient.execute(httpPost);

			if (response.getStatusLine().getStatusCode() == 200) {
				/*
				 * ################################
				 * 
				 * Send them to payment success page!
				 * 
				 * ################################
				 */
				redirectURI = "complete?faces-redirect=true";
				log.debug("Redirecting to: " + redirectURI);
				return redirectURI;
			} else if (response.getStatusLine().getStatusCode() == 500) {
				/*
				 * ################################
				 * 
				 * Send them to payment failed page!
				 * 
				 * ################################
				 */
				redirectURI = "/emailFail.xhtml?err="
						+ URLEncoder.encode(response.getStatusLine().getReasonPhrase(), "UTF-8")
						+ "&faces-redirect=true";
				log.debug("Redirecting to: " + redirectURI);
				return redirectURI;
			} else {
				/*
				 * ################################
				 * 
				 * Send them to payment failed page!
				 * 
				 * ################################
				 */
				redirectURI = "/fail.xhtml?err="
						+ URLEncoder.encode(response.getStatusLine().getReasonPhrase(), "UTF-8")
						+ "&faces-redirect=true";
				log.debug("Redirecting to: " + redirectURI);
				return redirectURI;
			}

		} catch (IOException e) {
			log.debug("IOException: " + e.getMessage());
			/*
			 * ################################
			 * 
			 * Send them to payment failed page!
			 * 
			 * ################################
			 */
			redirectURI = "/fail.xhtml?err=" + e.getMessage() + "&faces-redirect=true";
			return redirectURI;
		}
	}

	private List<NameValuePair> prepareFormData() {
		List<NameValuePair> params = new ArrayList<NameValuePair>();

		FacesContext context = FacesContext.getCurrentInstance();
		Map<String, String> formValues = context.getExternalContext().getRequestParameterMap();

		// Set invoice numbers
		for (Map.Entry<String, String> formData : formValues.entrySet()) {
			String key = formData.getKey();
			String value = formData.getValue();
			if (key.startsWith("invoice") && !value.isEmpty()) {
				params.add(new BasicNameValuePair(key, value));
			}
			if (key.startsWith("card_type") && !value.isEmpty()) {
				params.add(new BasicNameValuePair(key, value));
			}
		}

		params.add(new BasicNameValuePair(TOKEN_VALUE, paymentViewBean.getTokenValue()));
		params.add(new BasicNameValuePair(CLIENT_NAME, paymentViewBean.getClientName()));
		params.add(new BasicNameValuePair(AMOUNT, paymentViewBean.getAmount()));
		params.add(new BasicNameValuePair(NAME_ON_CARD, paymentViewBean.getNameOnCard()));
		params.add(new BasicNameValuePair(BILL_ADDR_1, paymentViewBean.getBillAddress1()));
		params.add(new BasicNameValuePair(BILL_ADDR_2, paymentViewBean.getBillAddress2()));
		params.add(new BasicNameValuePair(BILL_ADDR_3, paymentViewBean.getBillAddress3()));
		params.add(new BasicNameValuePair(BILL_COUNTRY, paymentViewBean.getBillCountry()));
		params.add(new BasicNameValuePair(BILL_STATE, paymentViewBean.getBillState()));
		params.add(new BasicNameValuePair(BILL_CITY, paymentViewBean.getBillCityTown()));
		params.add(new BasicNameValuePair(BILL_ZIP, paymentViewBean.getBillZipCode()));
		// params.add(new BasicNameValuePair(CARD_TYPE,
		// paymentViewBean.getCardType()));
		// params.add(new BasicNameValuePair(CARD_NUMBER,
		// paymentViewBean.getCardNumber()));
		// params.add(new BasicNameValuePair(CARD_CVC,
		// paymentViewBean.getCardCvC()));
		// params.add(new BasicNameValuePair(EXP_MONTH,
		// paymentViewBean.getExpMonth()));
		// params.add(new BasicNameValuePair(EXP_YEAR,
		// paymentViewBean.getExpYear()));
		params.add(new BasicNameValuePair(OTHER_COMMENTS, paymentViewBean.getOtherComments()));
		params.add(new BasicNameValuePair(CONTACT_NAME, paymentViewBean.getContactName()));
		params.add(new BasicNameValuePair(CONTACT_PHONE, paymentViewBean.getContactPhone()));
		params.add(new BasicNameValuePair(CONTACT_EMAIL, paymentViewBean.getContactEmail()));
		params.add(new BasicNameValuePair(CONTACT_ADDR_1, paymentViewBean.getContactAddress1()));
		params.add(new BasicNameValuePair(CONTACT_ADDR_2, paymentViewBean.getContactAddress2()));
		params.add(new BasicNameValuePair(CONTACT_ADDR_3, paymentViewBean.getContactAddress3()));
		params.add(new BasicNameValuePair(CONTACT_COUNTRY, paymentViewBean.getContactCountry()));
		params.add(new BasicNameValuePair(CONTACT_STATE, paymentViewBean.getContactState()));
		params.add(new BasicNameValuePair(CONTACT_CITY, paymentViewBean.getContactCityTown()));
		params.add(new BasicNameValuePair(CONTACT_ZIP, paymentViewBean.getContactZipCode()));

		return params;
	}

	public PaymentViewBean getPaymentViewBean() {
		return paymentViewBean;
	}

	public void setPaymentViewBean(PaymentViewBean paymentViewBean) {
		this.paymentViewBean = paymentViewBean;
	}

}
