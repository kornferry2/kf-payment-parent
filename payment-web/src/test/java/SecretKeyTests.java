import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;

import org.junit.Test;

import com.hps.integrator.entities.credit.HpsCardHolder;
import com.hps.integrator.entities.credit.HpsCharge;
import com.hps.integrator.entities.credit.HpsCreditCard;
import com.hps.integrator.infrastructure.HpsException;
import com.hps.integrator.services.HpsCreditService;
import com.hps.integrator.services.HpsServicesConfig;

public class SecretKeyTests {

	@Test
	public void can_charge_with_secret_key() throws HpsException {

		HpsServicesConfig config = new HpsServicesConfig();
		// config.setSecretAPIKey("pkapi_cert_GmDP0S5MlWfB55Bko2");

		HpsCreditCard card = new HpsCreditCard();
		card.setCvv(123);
		card.setExpMonth(12);
		card.setExpYear(2015);
		card.setNumber("xxxxxxxxxxxxxxxx");

		HpsCardHolder cardHolder = new HpsCardHolder();
		cardHolder.getAddress().setZip("750241234");

		HpsCreditService service = new HpsCreditService(config);
		HpsCharge charge = service.charge(new BigDecimal("10.00"), "usd", card, cardHolder, false, false, null, null);
		assertNotNull(charge);
	}

}
